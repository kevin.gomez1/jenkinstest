import hudson.EnvVars;
import hudson.slaves.EnvironmentVariablesNodeProperty;
import hudson.slaves.NodeProperty;
import hudson.slaves.NodePropertyDescriptor;
import hudson.util.DescribableList;
import jenkins.model.Jenkins;

public createGlobalEnvironmentVariables(String key, String value){

        Jenkins instance = Jenkins.getInstance();

        DescribableList<NodeProperty<?>, NodePropertyDescriptor> globalNodeProperties = instance.getGlobalNodeProperties();
        List<EnvironmentVariablesNodeProperty> envVarsNodePropertyList = globalNodeProperties.getAll(EnvironmentVariablesNodeProperty.class);

        EnvironmentVariablesNodeProperty newEnvVarsNodeProperty = null;
        EnvVars envVars = null;

        if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
            newEnvVarsNodeProperty = new hudson.slaves.EnvironmentVariablesNodeProperty();
            globalNodeProperties.add(newEnvVarsNodeProperty);
            envVars = newEnvVarsNodeProperty.getEnvVars();
        } else {
            envVars = envVarsNodePropertyList.get(0).getEnvVars();
        }
        envVars.put(key, value)
        instance.save()
}

def diffReturn(apps,String matinstance) {
    def appsDiff = []
    for (int i = 0; i < apps.size(); i++) {
        dir ('../helm-charts') {
            diffReturn = sh(
                returnStdout: true, 
                script: 'helm diff upgrade --reuse-values -n ' + matinstance + ' ' + apps[i] + ' ' + apps[i] 
            )
            if (diffReturn) {
                appsDiff.add(apps[i])
            }
        }
    }
    return appsDiff    
}

def upgradeApps(apps, String matinstance) {
    def appsToUpgrade = diffReturn(apps, matinstance)
    def stages = [failFast: true]
    for (int i = 0; i < appsToUpgrade.size(); i++) {
        def vmNumber = i
        stages["${appsToUpgrade[vmNumber]}"] = {
            stage("Upgrade apps") {
                    dir ('../helm-charts') {
                        labelledShell label: 'Helm upgrade', script: 'helm upgrade --install --reuse-values ' + appsToUpgrade[vmNumber] + ' ' + appsToUpgrade[vmNumber] + ' -n ' + matinstance           
                    }
                }
            }
        }
    return stages
}

def getRevision(apps, String matinstance) {
    appsRevisions = [:]
    for (int i = 0; i < apps.size(); i++) {
        revision = sh(
            returnStdout: true, 
            script: '''
                helm history ''' + apps[i] + ' -n ' + matinstance + ''' | grep deployed | awk ' {print $1} '
            '''
        )
        appsRevisions.put(apps[i], revision)
    }
    return appsRevisions
}

def rollbackApps(originalRevision, lastRevision) {
    appsToRollback = []
    originalRevision.each { k1, v1 ->
        def v2 = lastRevision[k1]
        if(v2>v1){
            appsToRollback.add(k1)
        }
    }
    return appsToRollback
}

return this 